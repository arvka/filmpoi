package trade.arka.iak.filmpoi;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by HP on 17/02/2018.
 */

public interface ApiService {
    String key = "9665dbe4442363e40e59ccf676bb75e9";

    @GET
    Call<ServiceResult> getTopRatedMovie(@Url String url);

    @GET
    Call<ServiceResult> getMovies(@Url String url);

    @GET
    Call<ReviewResult> getReview(@Url String url);
}
