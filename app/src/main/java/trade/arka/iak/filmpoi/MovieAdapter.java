package trade.arka.iak.filmpoi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by HP on 17/02/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private Context mContext;
    private List<MovieList> movieList;

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, year;
        public ImageView thumbnail;
        public String overview,backdropPath,posterPath,genres,originalTitle;
        public double rating;

        public MovieViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            year = (TextView) view.findViewById(R.id.year);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(),DetailMovie.class);
            intent.putExtra("title",title.getText());
            intent.putExtra("originaltitle",originalTitle);
            intent.putExtra("year",year.getText());
            intent.putExtra("backdropPath",backdropPath);
            intent.putExtra("posterPath",posterPath);
            intent.putExtra("overview",overview);
            intent.putExtra("rating",rating);
            intent.putExtra("genre",genres);
            mContext.startActivity(intent);
//            Toast.makeText(mContext,"Hallo",Toast.LENGTH_SHORT).show();
        }
    }


    public MovieAdapter(Context mContext, List<MovieList> movieList) {
        this.mContext = mContext;
        this.movieList = movieList;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.film_content_list, parent, false);

        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MovieViewHolder holder, int position) {
        MovieList movie = movieList.get(position);
        MovieViewHolder holderBind = holder;
        holder.title.setText(movie.getTitle());
        holder.year.setText(movie.getReleaseDate());
        Picasso.with(mContext)
                .load("https://image.tmdb.org/t/p/w185"+movie.getPosterPath())
                .into(holder.thumbnail);
        //set value
        holderBind.rating = movie.getVoteAverage();
        holderBind.backdropPath = movie.getBackdropPath();
        holderBind.overview = movie.getOverview();
        holderBind.posterPath = movie.getPosterPath();
        holderBind.originalTitle = movie.getOriginalTitle();
        String genres = "";
        for (int i = 0;i<movie.getGenreIds().size();i++){
            genres += movie.getGenreIds().get(i);
            if(i < movie.getGenreIds().size()-1){
                genres += ", ";
            }
        }
        holderBind.genres = genres;
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
