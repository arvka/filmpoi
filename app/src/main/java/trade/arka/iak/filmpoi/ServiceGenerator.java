package trade.arka.iak.filmpoi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HP on 28/02/2018.
 */

public class ServiceGenerator {
    public static ServiceGenerator instance = null;
    private ApiService service;
    String base_url = "https://api.themoviedb.org/3/";

    public ServiceGenerator() {
        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(ApiService.class);
    }

    private Retrofit.Builder createAdapter(){
        return new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create());
    }

    public static ServiceGenerator getInstance(){
        if(instance==null){
            instance = new ServiceGenerator();
        }
        return instance;
    }

    public ApiService getService() {
        return service;
    }
}
