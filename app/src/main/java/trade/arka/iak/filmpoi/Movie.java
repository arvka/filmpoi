package trade.arka.iak.filmpoi;

/**
 * Created by HP on 17/02/2018.
 */

public class Movie {
    private String title,thumbnail,synopsis,video,backdrop;
    private int year,movieID;
    private double rating;

    public Movie(String title, String thumbnail, String synopsis, String backdrop, int year, int movieID, double rating) {
        this.title = title;
        this.thumbnail = thumbnail;
        this.synopsis = synopsis;
        this.backdrop = backdrop;
        this.year = year;
        this.movieID = movieID;
        this.rating = rating;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
