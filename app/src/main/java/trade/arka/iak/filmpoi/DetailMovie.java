package trade.arka.iak.filmpoi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailMovie extends AppCompatActivity {

    ImageView backdropImage,posterImage;
    TextView title,originalTitle,tahun,deskripsi,genre,ratingTxt;
    RatingBar rating;
    Button trailer,favorite;
    RecyclerView rvReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        backdropImage = findViewById(R.id.detailBackdrop);
        posterImage = findViewById(R.id.detailPoster);
        title = findViewById(R.id.txtJudulFilm);
        originalTitle = findViewById(R.id.txtoriginalTitle);
        tahun = findViewById(R.id.txtTahunProduksi);
        genre = findViewById(R.id.txtGenre);
        deskripsi = findViewById(R.id.deskripsi);
        rating = findViewById(R.id.ratingBar);
        ratingTxt = findViewById(R.id.txtRating);
        trailer = findViewById(R.id.btnVisitWeb);
        favorite = findViewById(R.id.btnSetFavorite);
        rvReview = findViewById(R.id.recylerReview);

        final Bundle getMovie = getIntent().getExtras();
        Picasso.with(this)
                .load("https://image.tmdb.org/t/p/w500"+getMovie.getString("backdropPath"))
                .into(backdropImage);
        Picasso.with(this)
                .load("https://image.tmdb.org/t/p/w185"+getMovie.getString("posterPath"))
                .into(posterImage);
        title.setText(getMovie.getString("title"));
        originalTitle.setText("Original Title : "+getMovie.getString("originaltitle"));
        tahun.setText("Release Date : "+getMovie.getString("year"));
        genre.setText("Genre : "+getMovie.getString("genre"));
        ratingTxt.setText("Rating : "+String.valueOf(getMovie.getDouble("rating")));
        rating.setRating((float) getMovie.getDouble("rating")/2);
        deskripsi.setText(getMovie.getString("overview"));

        trailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/results?search_query="+ getMovie.getString("title")+" trailer")));
            }
        });
    }
}
