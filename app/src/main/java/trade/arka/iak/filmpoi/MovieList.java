package trade.arka.iak.filmpoi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 23/02/2018.
 */

public class MovieList {
    @SerializedName("vote_count")
    @Expose
    private Integer voteCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("video")
    @Expose
    private Boolean video;
    @SerializedName("vote_average")
    @Expose
    private Double voteAverage;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("popularity")
    @Expose
    private Double popularity;
    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    @SerializedName("original_language")
    @Expose
    private String originalLanguage;
    @SerializedName("original_title")
    @Expose
    private String originalTitle;
    @SerializedName("genre_ids")
    @Expose
    private List<Integer> genreIds = null;
    @SerializedName("backdrop_path")
    @Expose
    private String backdropPath;
    @SerializedName("adult")
    @Expose
    private Boolean adult;
    @SerializedName("overview")
    @Expose
    private String overview;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getVideo() {
        return video;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public List<String> getGenreIds() {
        List<String> genres = new ArrayList<String>();
        for ( Integer genre : genreIds ) {
            switch (genre){
                case 28 :
                    genres.add("Action");
                    break;
                case 12 :
                    genres.add("Adventure");
                    break;
                case 16 :
                    genres.add("Animation");
                    break;
                case 35 :
                    genres.add("Comedy");
                    break;
                case 80 :
                    genres.add("Crime");
                    break;
                case 99 :
                    genres.add("Documentary");
                    break;
                case 18 :
                    genres.add("Drama");
                    break;
                case 10751 :
                    genres.add("Family");
                    break;
                case 14 :
                    genres.add("Fantasy");
                    break;
                case 36 :
                    genres.add("History");
                    break;
                case 27 :
                    genres.add("Horror");
                    break;
                case 10402 :
                    genres.add("Music");
                    break;
                case 9648 :
                    genres.add("Mystery");
                    break;
                case 10749 :
                    genres.add("Romance");
                    break;
                case 878 :
                    genres.add("Science Fiction");
                    break;
                case 10770 :
                    genres.add("TV Movie");
                    break;
                case 53 :
                    genres.add("Thriller");
                    break;
                case 10752 :
                    genres.add("War");
                    break;
                default:
                    genres.add("");
                    break;
            }
        }
        return genres;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
